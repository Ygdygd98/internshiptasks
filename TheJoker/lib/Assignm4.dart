import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/rendering.dart';

class Todo {
  final String messenger;
  final String title;
  final String description;


  const Todo(this.messenger,this.title, this.description);
}

void main() {
  runApp(
    MaterialApp(
      title: 'Passing Data',
      home: TodosScreen(
        todos: List.generate(
        20,
            (i) => Todo(
          'Messenger $i',
          'Title of Email $i',
          'Hello Mr. $i \n\n My name is Messenger $i, I am writing this email to you to ask regarding the app that I had discussed with you in our prior email',
        ),
      )
      ),
    ),
  );
}

class TodosScreen extends StatelessWidget {
  const TodosScreen({Key? key, required this.todos}) : super(key: key);

  final List<Todo> todos;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pinkAccent,
        title: new Text('Inbox'),

        actions: [
          // action button
          IconButton(
            icon: Icon( Icons.search ),
            onPressed: () { },
          ),
          IconButton(
            icon: Icon( Icons.check_circle_rounded ),
            onPressed: () { },
          ),
        ],
        leading: IconButton(
          icon: Icon( Icons.menu ),
          onPressed: () { },
        ),
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return Column(
            children: <Widget>[
              GestureDetector(
                onTap: ()  {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => const DetailScreen(),
                      // Pass the arguments as part of the RouteSettings. The
                      // DetailScreen reads the arguments from these settings.
                      settings: RouteSettings(
                        arguments: todos[index],
                      ),
                    ),
                  );
                },
                child: Container(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Flexible(
                        child:
                        InkWell(
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding:
                                  const EdgeInsets.fromLTRB(12.0, 12.0, 12.0, 6.0),
                                  child: Text(
                                    todos[index].messenger,
                                    style: TextStyle(
                                        fontSize: 20.0, fontWeight: FontWeight.bold),
                                  ),

                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.fromLTRB(12.0, 0, 12.0, 0),
                                  child: Text(
                                    todos[index].title,
                                    style: TextStyle(fontSize: 14.0),
                                  ),
                                ),
                                Padding(
                                  padding:
                                  const EdgeInsets.fromLTRB(12.0, 6.0, 12.0, 12.0),
                                  child: Text(
                                    todos[index].description,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                    softWrap: false,
                                    style: TextStyle(fontSize: 14.0),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Text(
                              "5m",
                              style: TextStyle(color: Colors.grey),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(12.0, 40.0, 12.0, 12.0),
                              child: Icon(
                                Icons.star_border,
                                size: 20.0,
                                color: Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Divider(
                height: 0.5,
                color: Colors.grey,
              )
            ],
          );
        },
        itemCount: 7,
      ),
    );
  }
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final todo = ModalRoute.of(context)!.settings.arguments as Todo;

    return Scaffold(
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Stack(
                children: [

                  Column(
                    children: [new Container(
                      width: 500,
                      height:300,
                      color: Colors.grey[200],
                      child: new Image.network(
                        'https://upload.wikimedia.org/wikipedia/commons/thumb/6/64/Mycenaean_stirrup_vase_Louvre_AO19201.jpg/220px-Mycenaean_stirrup_vase_Louvre_AO19201.jpg',
                        fit: BoxFit.fill,
                      ),
                    ),],
                  ),
                  Align(
                    alignment: Alignment.bottomRight ,
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(right: 30.0),
                          child: Container(
                              width: 35,
                              height:325,
                              child: Align(
                                alignment: Alignment.bottomRight,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(100)),
                                  color: Colors.blue,
                                  onPressed: () {
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(right:20.0),
                                    child: Icon(Icons.favorite_outlined),
                                  ),
                                ),
                              )
                          ),
                        ),
                      ],
                    ),
                  )
      ]
                 ),

                  Padding(
                    padding: const EdgeInsets.only( left:25,bottom:10,right:25),
                    child: Row(
                      children: [
                        Column(
                          children: [
                            Text('Artifact (archaeology)', style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),)
                          ],
                        )
                      ],
                    ),
                  ),
              Padding(
                padding: const EdgeInsets.only( left:25,bottom:10,right:25),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Text('From Wikipedia, the free encyclopedia', style: TextStyle(fontSize: 10),)
                      ],
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only( left:25,right:25),
                child: Divider(
                    color: Colors.black
                ),
              ),
              Padding(
                padding: const EdgeInsets.only( left:25,bottom:10,right:25),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Text('Introduction',style: TextStyle(fontWeight: FontWeight.bold),)
                      ],
                    )
                  ],
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(top:10.0, left:25,bottom:10,right:25),
                  child: new Container(
                    padding: new EdgeInsets.only(right: 13.0),
                    child: new Text(
                      'Artifact is the general term used in archaeology, while in museums the equivalent general term is normally "object", and in art history perhaps artwork or a more specific term such as "carving". The same item may be called all or any of these in different contexts, and more specific terms will be used when talking about individual objects, or groups of similar ones.',
                      style: new TextStyle(
                        fontSize: 10.0,
                        fontFamily: 'Roboto',
                        color: new Color(0xFF212121),
                      ),
                    ),
                  ),
                ),
              ),
              Flexible(
                child: Padding(
                  padding: const EdgeInsets.only(top:10.0, left:25,bottom:10,right:25),
                  child: new Container(
                    padding: new EdgeInsets.only(right: 13.0),
                    child: new Text(
                      'Artifacts exist in many different forms and can sometimes be confused with ecofacts and features; all three of these can sometimes be found together at archaeological sites. They can also exist in different types of context depending on the processes that have acted on them over time. A wide variety of analyses take place to analyze artifacts and provide information on them. However, the process of analyzing artifacts through scientific archaeology can be hindered by the looting and collecting of artifacts, which sparks ethical debate.',
                      style: new TextStyle(
                        fontSize: 10.0,
                        fontFamily: 'Roboto',
                        color: new Color(0xFF212121),
                      ),
                    ),
                  ),
                ),
              ),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  margin: const EdgeInsets.only(top:0,left:40 ,right:40 ,bottom: 15),
                  width: double.infinity,
                  height:45,
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(25)),
                    color: Colors.blue,
                    onPressed: () {
                    },
                    child: Text("LIHAT MAKLUMAT LENGKAP",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.white)),
                  ),
                ),

              ),
                ],

        ),

    );
  }
}