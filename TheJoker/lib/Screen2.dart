import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:internship/PageUtama.dart';
import 'package:internship/main.dart';

import 'package:flutter/material.dart';

class SecondScreen extends StatefulWidget {
  String userName;
  bool value;

  SecondScreen({
    required this.userName,
    required this.value,
  });

  @override
  _SecondScreenState createState() => _SecondScreenState();
}

class _SecondScreenState extends State<SecondScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          ListView(
            children: [
              Padding(
                  padding: const EdgeInsets.only(left: 15.0,top:20,bottom:20),
                  child: Text('Name Penuh')
              ),
              Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(left:15.0,top: 0),
                    child: Text('${widget.userName}',style: TextStyle(fontSize: 20.0), ),
                  ),
                ],
              ),
              Row(
                children: [
                      Padding(
                        padding: const EdgeInsets.only(left:15.0,top: 15),
                        child: Text('Bilik'),
                      )
                    ],
              ),
              Row(
                children: [
                  if (widget.value == true) ...[
                    Padding(
                      padding: const EdgeInsets.only(left:15.0,top: 15),
                      child: Text('B12-11',style: TextStyle(fontSize: 20.0),),
                    ),
                  ] else ...[
                    Padding(
                      padding: const EdgeInsets.only(left:15.0,top: 15),
                      child: Text('Penginapan Tidak Diperlukan',style: TextStyle(fontSize: 20.0),),
                    ),
                  ],
                ],
              ),
            ],

          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.only(top:0,left:20 ,right:20 ,bottom: 15),
              width: double.infinity,
              height:50,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10)),
                color: Colors.blue,
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MainScreen(
                      ),
                    ),
                  );
                },
                child: Text("HALAMAN UTAMA",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white)),
              ),
            ),

          ),
        ],
      )
    );
  }
}