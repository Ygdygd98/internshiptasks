import 'package:http/http.dart' as http;
import 'package:internship/shopping_app/Model.dart';
import 'package:internship/shopping_app/home_view.dart';

class HttpService {
  static Future<List<ProductsModel>> fetchProducts() async {
    var response =
    await http.get(Uri.parse("https://fakestoreapi.com/products"));
    if (response.statusCode == 200) {
      var data = response.body;
      return productsModelFromJson(data);
    } else {
      throw Exception();
    }
  }
}