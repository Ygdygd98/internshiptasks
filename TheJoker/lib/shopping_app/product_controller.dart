import 'package:get/get.dart';
import 'package:internship/shopping_app/Model.dart';
import 'package:internship/shopping_app/http_service.dart';
import 'package:internship/shopping_app/home_view.dart';


class ProductController extends GetxController {
  var isLoading = true.obs;
  var productList = [].obs;


  @override
  void onInit() {
    fetchProducts();
    super.onInit();
  }

  void fetchProducts() async {
    try {
      isLoading(true);
      var products = await HttpService.fetchProducts();
      if (products != null) {
        productList.value = products;
      }
    } finally {
      isLoading(false);
    }
  }
}