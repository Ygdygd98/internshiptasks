import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:internship/shopping_app/Model.dart';
import 'package:internship/shopping_app/http_service.dart';
import 'package:internship/shopping_app/product_controller.dart';
import 'package:get/get.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class HomePageView extends StatelessWidget {
  final ProductController productController = Get.put(ProductController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        elevation: 0,
        leading: const Icon(Icons.arrow_back_ios),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.shopping_cart),
          ),
        ],
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(16),
            child: Row(
              children: [
                const Expanded(
                  child: Text(
                    "ShopMe",
                    style: TextStyle(
                        fontFamily: "avenir",
                        fontSize: 32,
                        fontWeight: FontWeight.w900),
                  ),
                ),
              ],
            ),
          ),
          Expanded(
            child: Obx(
                  () {
                if (productController.isLoading.value) {
                  return Center(child: CircularProgressIndicator());
                } else
                  return StaggeredGridView.countBuilder(
                    crossAxisCount: 2,
                    itemCount: productController.productList.length,
                    crossAxisSpacing: 16,
                    mainAxisSpacing: 16,
                    itemBuilder: (context, index) {
                      return Card(
                          child: Column(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: GestureDetector(
                                onTap: (){
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => ItemDetailScreen(ProductIndex: index),
                                      // Pass the arguments as part of the RouteSettings. The
                                      // DetailScreen reads the arguments from these settings.
                                      settings: RouteSettings(
                                        arguments: productController.productList[index],
                                      ),
                                    ),
                                  );
                                },
                                child: Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: 200,
                                  decoration: BoxDecoration(
                                    image: DecorationImage(
                                      fit: BoxFit.fill,
                                      image: NetworkImage(productController.productList[index].image),
                                    ),
                                  ),
                                ),
                              )
                            ),
                      Padding(
                        padding: const EdgeInsets.only(left: 3.0,right: 3.0),
                        child: SizedBox(height:30,
                          child: Text(productController.productList[index].title,maxLines: 2,
                            overflow: TextOverflow.ellipsis,),
                        ),
                      ),
                            Padding(
                              padding: const EdgeInsets.only(top:8.0),
                              child: SizedBox(height:20,
                                child: Center(
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.circular(10),
                                    child: Container(
                                      width: 55,
                                      color: Colors.green,
                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        children: [
                                          Column(children: [Text(productController.productList[index].rating.rate.toString(),style: TextStyle(fontSize: 15,color: Colors.white)),
                                          ]),

                                          Column(
                                                children: [Icon(Icons.star_outlined, size: 15,color: Colors.white,
                                                )]
                                            ),

                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Column(children: [Text("\$",style: TextStyle(fontSize: 30)),
                                  ]),
                                  Column(
                                      children: [Text(productController.productList[index].price.toString(),style: TextStyle(fontSize: 30),
                                      )]
                                  )
                                ],
                              ),
                            ),
                          ]
                          ));
                    },
                    staggeredTileBuilder: (index) => StaggeredTile.fit(1),
                  );
              },
            ),
          ),
        ],
      ),
    );
  }
}

class ItemDetailScreen extends StatefulWidget {

  final ProductIndex;


  ItemDetailScreen({
    required this.ProductIndex,

  });

  @override
  State<ItemDetailScreen> createState() => _ItemDetailScreenState();
}

class _ItemDetailScreenState extends State<ItemDetailScreen> {
  @override
  Widget build(BuildContext context) {
    final ProductController productController = Get.put(ProductController());
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        elevation: 0,
        leading: const Icon(Icons.arrow_back_ios),
        actions: [
          IconButton(
            onPressed: () {},
            icon: const Icon(Icons.shopping_cart),
          ),
        ],
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [


                Column(
                  children: [new Container(
                    width: 500,
                    height:250,
                    color: Colors.grey[200],
                    child: new Image.network(
                      (productController.productList[widget.ProductIndex].image)
                    ),
                  ),],
                ),



            Padding(
              padding: const EdgeInsets.only(top:10.0, left:25,bottom:0,right:25),
              child: new Container(
                padding: new EdgeInsets.only(right: 13.0),
                child: new Text(
                    (productController.productList[widget.ProductIndex].title),style: TextStyle(fontWeight: FontWeight.bold,fontSize: 20)
                ),
              ),
            ),

          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top:0.0, left:25,bottom:0,right:25),
              child: new Container(
                height: 20,
                padding: new EdgeInsets.only(right: 13.0),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Text(
                            'Category: ',style: TextStyle(fontWeight: FontWeight.bold)
                        ),

                      ],
                    ),
                    Column(
                      children: [
                        Text(
                            (productController.productList[widget.ProductIndex].category),style: TextStyle(fontWeight: FontWeight.bold)
                        ),

                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Flexible(
            child: Padding(
              padding: const EdgeInsets.only(top:0.0, left:25,bottom:0,right:25),
              child: new Container(
                height: 20,
                padding: new EdgeInsets.only(right: 13.0),
                child: Row(
                  children: [
                    Column(
                      children: [
                        Text(
                            'Price: \$',style: TextStyle(fontWeight: FontWeight.bold)
                        ),

                      ],
                    ),
                    Column(
                      children: [
                        Text(
                            (productController.productList[widget.ProductIndex].price.toString()),style: TextStyle(fontWeight: FontWeight.bold)
                        ),

                      ],
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only( left:25,right:25),
            child: Divider(
                color: Colors.black
            ),
          ),
          Padding(
            padding: const EdgeInsets.only( left:25,bottom:0,right:25),
            child: Center(
              child: RatingBar.builder(
                initialRating: productController.productList[widget.ProductIndex].rating.rate,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.amber,
                  size: 5,
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left:175,bottom:0),
            child: Row(
              children: [
                Column(
                  children: [
                    Text(
                        'Rating: ',style: TextStyle(fontWeight: FontWeight.bold)
                    ),

                  ],
                ),
                Column(
                  children: [
                    Text(
                        (productController.productList[widget.ProductIndex].rating.rate.toString()),style: TextStyle(fontWeight: FontWeight.bold)
                    ),

                  ],
                )
              ],
            ),
          ),
          Padding(
              padding: const EdgeInsets.only(top:15.0, left:25,bottom:0,right:25),
              child: new Container(
                height: 110,
                padding: new EdgeInsets.only(right: 13.0),
                child: new Text(
                    (productController.productList[widget.ProductIndex].description)
                ),
              ),
            ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              margin: const EdgeInsets.only(top:0,left:40 ,right:40 ,bottom: 0),
              width: double.infinity,
              height:45,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25)),
                color: Colors.green,
                onPressed: () {
                },
                child: Text("ADD TO CART",style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold,color: Colors.white)),
              ),
            ),

          ),
        ],

      ),

    );
  }
}