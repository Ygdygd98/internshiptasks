
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:internship/Screen2.dart';


class Assignment3 extends StatefulWidget {
  @override
  _StateAssignment3 createState() => new _StateAssignment3();
}

class _StateAssignment3 extends State<Assignment3> {
  TextEditingController input = TextEditingController();
  bool valuefirst = false;


  final _formKey = GlobalKey<FormState>();
  set value(bool value) {}
  @override

  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
        backgroundColor: Color(0xFF00897B),
        body: Center(
          child: Padding(
            padding: const EdgeInsets.only(top:125.0),
            child: Stack(
              children: [
                ListView(
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(0),
                      child: CircleAvatar(
                          radius: 50.0,
                          backgroundColor: Colors.transparent,
                          child: CircleAvatar(
                            radius: 50.0,
                            backgroundImage:  AssetImage("asset/images/joker.jpg"),
                        ),
                      ),
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(0),
                          child: Center(
                            child: Text(
                              'The Joker',
                              style: TextStyle(
                                fontFamily: 'Pacifico',fontSize: 38,color: Colors.white,fontWeight: FontWeight.bold,
                            ),
                        ),
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(0),
                          child: Center(
                            child: Text(
                              'ALL  IT  TAKES  IS  A  LITTLE  PUSH',
                              style: TextStyle(
                                  fontSize: 15,color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
        Padding(
            padding: const EdgeInsets.only(top:20,left:15,bottom:10,right:15),
            child: Container(
              width: 200,
              height: 74,
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.all(5.0),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        const ListTile(
                          leading: Icon(Icons.phone, size: 25, color:Color(0xFF00897B)),
                          title: Text('+60127294912' ,style: TextStyle(
                            fontSize: 16,color:Color(0xFF00897B),
                          ),),
                        ),
                      ],
                    ),
                  ),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)),
                ),
            ),
        ),
                    Padding(
                      padding: const EdgeInsets.only(top:5,left:15,bottom:10,right:15),
                      child: Container(
                        width: 200,
                        height: 74,
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: <Widget>[
                                const ListTile(
                                  leading: Icon(Icons.mail, size: 25, color:Color(0xFF00897B)),
                                  title: Text('joker@gmail.com' ,style: TextStyle(
                                    fontSize: 16,color:Color(0xFF00897B),
                                  ),),
                                ),
                              ],
                            ),
                          ),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                        ),
                      ),
                    )
      ]
            ),
    ]
        ),
          )

    ));
  }
}
