
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:internship/Assignm4.dart';
import 'package:internship/Assignment3.dart';
import 'package:internship/Screen2.dart';
import 'package:internship/shopping_app/home_view.dart';

void main() {
  runApp(new MaterialApp(
    home: HomePageView(),
    debugShowCheckedModeBanner: false,
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const appTitle = 'Welcome to Flutter';

    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      home: Scaffold(
        appBar: AppBar(
          title: const Text(appTitle),
        ),
        body: const MyCustomForm2(),
      ),
    );
  }
}

// Create a Form widget.
class MyCustomForm2 extends StatefulWidget {
  const MyCustomForm2({Key? key}) : super(key: key);

  @override
  MyCustomFormState2 createState() {
    return MyCustomFormState2();
  }
}

// Create a corresponding State class.
// This class holds data related to the form.
class MyCustomFormState2 extends State<MyCustomForm2> {
  // Create a global key that uniquely identifies the Form widget
  // and allows validation of the form.
  //
  // Note: This is a GlobalKey<FormState>,
  // not a GlobalKey<MyCustomFormState>.
  TextEditingController input = TextEditingController();
  bool valuefirst = false;


  final _formKey = GlobalKey<FormState>();
  set value(bool value) {}

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    return Stack(
      children: [
        ListView(
          children: [
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextFormField(
                      controller: input,
                      decoration: InputDecoration(
                        labelText: 'Enter Name',
                        hintText: 'Enter Your Name',),
                      // The validator receives the text that the user has entered.
                      validator: (value) {
                        if (value == null || value.isEmpty) {
                          return 'Sila isikan Maklumat yang diminta';
                        }
                        return null;
                      },
                    ),
                  ),
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(left:15.0),
                        child: Text('Adakah anda memerlukan penginapan? ',style: TextStyle(fontSize: 17.0), ),
                      ),
                    ],
                  ),
                  Row(
                    children: [
                      Column(
                        children: [
                          Checkbox(
                            value: valuefirst,
                            onChanged: (value) {
                              setState(() {
                                valuefirst = true;
                              });
                            },
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Text('Ya',style: TextStyle(fontSize: 17),)
                        ],
                      )
                    ],


                  ),
                ],
              ),
            ),
          ],
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            margin: const EdgeInsets.only(top:0,left:20 ,right:20 ,bottom: 15),
            width: double.infinity,
            height:50,
            child: RaisedButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              color: Colors.blue,
              onPressed: () {
                print(input.text);
                if (input.text != ""){
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => SecondScreen(
                        userName: input.text,
                        value: valuefirst,
                      ),
                    ),
                  );
                }else{

                  showDialog(
                      context: context,
                      builder: (BuildContext context) => new AlertDialog(
                        title: new Text('Warning'),
                        content: new Text('Sila isikan maklumat yang diperlukan'),
                        actions: <Widget>[
                          new  FlatButton(
                            color: Colors.blue,
                            onPressed: (){
                              Navigator.pop(context);
                              // Validate returns true if the form is valid, or false otherwise.
                              if (_formKey.currentState!.validate()) {
                                // If the form is valid, display a snackbar. In the real world,
                                // you'd often call a server or save the information in a database.
                                ScaffoldMessenger.of(context).showSnackBar(
                                  const SnackBar(content: Text('Processing Data')
                                  ),
                                );
                              }
                            },
                            child: Text(
                              "Retry",
                              style: TextStyle(color: Colors.white, fontSize: 20),
                            ),
                          )
                        ],
                      ));
                }

              },
              child: Text("SUBMIT",style: TextStyle(fontSize: 20,fontWeight: FontWeight.bold,color: Colors.white)),
            ),
          ),

        ),
      ],
    );
  }
}