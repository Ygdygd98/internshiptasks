# internship

A new Flutter project.

## Getting Started

This project is used to compile various tasks given to me as a means of learning the basics of
dart and flutter.

Contains:

MyApp : A simple form, tickbox and submit button where if form is not filled, will receive
        popup warning, stating the user that a necessary field is not filled and the form
        is highlighted red, stating that it is compulsary for user to fill.

Assignment3 : A simple profile containing a CircleAvatar to ouput an image, and 2 words below,
            one being a different font, and two cards to display information(Phone Number and Email)


